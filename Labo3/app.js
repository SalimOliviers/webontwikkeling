const express = require('express');
const ejs = require('ejs');
const persoon = require('./views/persoon.json');
const Pokedex = require('pokedex');
      pokemon = new Pokedex();
let arrayOfPokemon = [];
for (let index = 10; index <= 14; index++) {
  arrayOfPokemon.push(pokemon.pokemon(index))
}

const app = express();
app.set('port', process.env.PORT || 3000);
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));

app.get('/', (req,res) => {
  {res.render('index',{'name': persoon.name, 'image': persoon.image})}
});

app.get('/pikachu', (req,res) => {
  {res.render('pokemon',{'pokemon': pokemon.pokemon('pikachu')})}
});

app.get('/raichu', (req,res) => {
  {res.render('pokemon',{'pokemon': pokemon.pokemon('raichu')})}
});

app.get('/pokemons', (req,res) => {
  {res.render('pokemons',{'pokemon': arrayOfPokemon})}
});

for (let index = 100; index <= 109; index++) {
  console.log(pokemon.pokemon(index));
  app.get('/' + pokemon.pokemon(index).name, (req,res) => {
    {res.render('pokemon',{'pokemon': pokemon.pokemon(index)})}
  });
}

app.listen(app.get('port'), () => {
  console.log(`Express started on http://localhost:${
    app.get('port')}; press Ctrl-C to terminate.`);
});