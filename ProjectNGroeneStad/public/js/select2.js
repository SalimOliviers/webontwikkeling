let data;

function setCookie(cName,cValue,cExdays) {
  let d = new Date();
  d.setTime(d.getTime() + (cExdays*24*60*60*1000));
  var expires = "expires=" + d.toGMTString();
  document.cookie = cName + "=" + cValue + ";" + expires + ";path=/";
}

function getCookie(cName) {
  let name = cName + "=";
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(';');
  for(let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function checkCookie(cValue) {
  let regio=getCookie("regio");
  if (regio != "") {
    alert("cookie van " + regio + " is er al!");
  } else {
    alert("Er is geen cookie van " + regio + "beschikbaar!")
    setCookie("regio", cValue, 30);
    
  }
}

$(document).ready(function()
{
  let regios = 
  [
    "Berchem", "Deurne", "Mechelen", "Borgerhout"
  ];
  $("#regios").select2({
    data:regios
  })
});

$('#regios').on('select2:select', function (e) {
   data = e.params.data;
  });

function cokkieToevoegen(){
  checkCookie(data.text);
  let text = [data.text]
  $(document).ready(function()
{
  $("#favoriten").select2({
    data:text
  })
});
}
document.getElementById("addCookie").addEventListener('click', cokkieToevoegen);

function deleteCookie(){
  document.cookie = "regio= ; expires = Thu, 01 Jan 1970 00:00:00 GMT; path=/"
  console.log(document.cookie)
  $("#favoriten option[value='"+data.text+"']").remove();
}

document.getElementById("deleteCookie").addEventListener('click', deleteCookie);