const express = require('express');
const ejs = require('ejs');

const app = express();
app.set('port', process.env.PORT || 3000);
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));

app.get('/', (req, res) => 	{
    res.render('index', {name: "pikachu"});
});

app.get('/contact', (req, res) => 	{
    res.render('contact');
});


app.listen(app.get('port'), () => {
  console.log(`Express started on http://localhost:${
    app.get('port')}; press Ctrl-C to terminate.`);
});