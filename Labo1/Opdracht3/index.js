const http = require('http');
let i = 0;
const server = http.createServer((request, response) => {
    i++;
    response.writeHead(200, {'Content-Type':'application/json'});
    response.write(JSON.stringify({voornaam: "jan", achternaam: "Alleman", called: i}));
    response.end();
});

server.listen(1337);