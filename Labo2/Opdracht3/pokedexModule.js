const Pokedex = require('pokedex'),
      pokedex = new Pokedex();


 exports.printPokemonBetween = function(from,to){
    for (let index = from; index <= to; index++) {
        console.log(index + ":" + pokedex.pokemon(index).name)
    }
}